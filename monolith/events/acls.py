import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):

    url = f"https://api.pexels.com/v1/search?query={city}{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    photo = json.loads(response.content)
    try:
        d = {
            "photo_url": photo["photos"][0]["src"]["original"],
        }
    except ValueError:
        d = {"photo_url": None}
    return d


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    latlon = json.loads(response.content)

    lat = latlon[0]["lat"]
    lon = latlon[0]["lon"]
    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response2 = requests.get(url2)
    temp = json.loads(response2.content)
    # temp
    # descrip`
    try:
        d = {
            "weather": {
                "temp": str(
                    int((temp["main"]["temp"] - 273.15) * (9 / 5) + (32))
                )
                + " F",
                "description": temp["weather"][0]["description"],
            }
        }
    except ValueError:
        d = {"weather": None}
    return d
